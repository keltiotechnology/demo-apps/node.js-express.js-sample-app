# Nodejs sample app with Express.js

This sample application generated using ```express-generator``` - a tool to quickly create an application skeleton. 
See more [here](https://expressjs.com/en/starter/generator.html).

## I. NPM commands

The application is running on port 3000 by default.

### Run application

#### A. Debug mode

On MacOS or Linux, run the app with this command:

```bash
DEBUG=express-sample-app:* npm start
```

On Windows Command Prompt, use this command:

```bash
set DEBUG=express-sample-app:* & npm start
```

On Windows PowerShell, use this command:

```bash
PS> $env:DEBUG='express-sample-app:*'; npm start
```

#### B. Without Debug mode

```bash
npm start
```

## II. Using Docker

### 1. Build docker image

Supported build args:

| ARG          | DEFAULT_VALUE |
|--------------|---------------|
| NODE_VERSION | 12.2.0        |

```bash
docker build -t express:v0.1 . 
```

### 2. Run container

Run application

```bash
docker container run -p 80:3000 express:v0.1
```

To run in Debug mode

```bash
docker container run -p 80:3000 -e DEBUG=express-sample-app:* express:v0.1
```
