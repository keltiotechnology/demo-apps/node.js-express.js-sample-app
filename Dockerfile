ARG NODE_VERSION=12.2.0-slim

FROM node:$NODE_VERSION
ARG NPM_ARGS
ENV NPM_ARGS=${NPM_ARGS}

# Create user, http://redhatgov.io/workshops/security_containers/exercise1.2/
RUN groupadd -r swuser -g 433 && \
    useradd -u 431 -r -g swuser -s /sbin/nologin -c "Docker image user" swuser

RUN mkdir /home/swuser && \
    chown -R swuser:swuser /home/swuser

USER swuser
WORKDIR /home/swuser

# Copy package.json and package-lock-json
COPY --chown=swuser:swuser package*.json ./

# Install dependencies
RUN npm install

COPY --chown=swuser:swuser . .

CMD ["npm", "start"]
